<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class SubscribersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
				
				$this->validate($request, [
					'NewsletterEmail' => 'required|email',
					'NewsletterName' => 'required'
				]);
			
        if(Mail::send('emails.subscribe', [], function($message) use ($request)
				{
						$message->to( $request->input('NewsletterEmail') , $request->input('NewsletterName'))
										->subject('Welcome!')
										->from('presiden@gov.id', 'Presiden');
						
				})){
					
						$return = [
								'status' => 'success'
						];
					
				} else {
					
						$return = [
								'status' => 'error',
								'message' => Mail::failure()
						];
					
				}
				
				return response()->json($return);
					
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
